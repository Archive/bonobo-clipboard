#include <config.h>

#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-moniker-util.h>
#include <bonobo/bonobo-listener.h>
#include <bonobo/bonobo-event-source.h>

#include <bonobo/bonobo-stream-memory.h>
#include <bonobo/bonobo-stream-client.h>

#include <bonobo/bonobo-exception.h>

#include <bonobo/bonobo-persist-stream.h>

#include <bonobo/bonobo-clipboard-util.h>

static Bonobo_Clipboard get_clipboard_moniker ();
static Bonobo_PersistStream get_xclipboard_moniker ();

static void do_bonobo_clipboard_test ();
static void listener_cb (BonoboListener    *listener,
			 char              *event_name,
			 CORBA_any         *any,
			 CORBA_Environment *ev,
			 gpointer           user_data);

static void do_xclipboard_test ();

static Bonobo_Clipboard get_clipboard_moniker ()
{
    CORBA_Environment ev;
    Bonobo_Clipboard clipboard;

    g_warning ("Resolving clipboard:");

    CORBA_exception_init (&ev);
    clipboard = bonobo_get_object ("clipboard:",
				   "IDL:Bonobo/Clipboard:1.0",
				   &ev);
    if (BONOBO_EX (&ev))
	g_error ("clipboard: resolve failed: %s",
		 bonobo_exception_get_text (&ev));
    CORBA_exception_free (&ev);

    return clipboard;
}

static Bonobo_PersistStream get_xclipboard_moniker ()
{
    CORBA_Environment    ev;
    Bonobo_PersistStream pstream;

    g_warning ("Resolving xclipboard:");
    
    CORBA_exception_init (&ev);
    pstream = bonobo_get_object ("xclipboard:",
				 "IDL:Bonobo/PersistStream:1.0",
				 &ev);
    if (BONOBO_EX (&ev))
	g_error ("xclipboard: resolve failed: %s",
		 bonobo_exception_get_text (&ev));
    CORBA_exception_free (&ev);
    
    return pstream;
}

static void listener_cb (BonoboListener    *listener,
			 char              *event_name,
			 CORBA_any         *any,
			 CORBA_Environment *ev,
			 gpointer           user_data)
{
    printf ("listener_cb: %s\n", event_name);
    
    if (strcmp (bonobo_event_idl_path (event_name), "Bonobo/Clipboard") == 0)
    {
	if (strcmp (bonobo_event_kind (event_name), "changed") == 0 &&
	    bonobo_event_subtype (event_name) == NULL)
	    printf ("Clipboard change detected\n");
    }
}

static void modify_dummy_component_state (Bonobo_Unknown component)
{
    CORBA_Environment     ev;
    Bonobo_PersistStream  pstream;
    BonoboStream         *memstream;
    Bonobo_Stream         stream;
    gchar                *new_state = "Internal state is modified";
    
    memstream = bonobo_stream_mem_create (new_state, 1024, FALSE, TRUE);
    stream = bonobo_object_corba_objref (BONOBO_OBJECT (memstream));
    
    CORBA_exception_init (&ev);
    pstream = Bonobo_Unknown_queryInterface (component,
					     "IDL:Bonobo/PersistStream:1.0",
					     &ev);
    Bonobo_PersistStream_load (pstream, stream, "", &ev);
    CORBA_exception_free (&ev);
}

static void do_bonobo_clipboard_test ()
{
    Bonobo_Clipboard  clipboard = get_clipboard_moniker ();
    Bonobo_Unknown    put_me_on_clipboard;
    CORBA_Environment ev;

    bonobo_event_source_client_add_listener (clipboard, listener_cb,
					     NULL, NULL, NULL);
					     
    printf ("Clearing the clipboard\n");

    bonobo_clipboard_set (CORBA_OBJECT_NIL,
			  CORBA_OBJECT_NIL,
			  NULL);
    
    printf ("Setting the clipboard to something via the ClipboardStore\n");

    CORBA_exception_init (&ev);
    put_me_on_clipboard = bonobo_get_object ("OAFIID:Cactus_Test_0",
					     "IDL:Bonobo/Unknown:1.0",
					     &ev);
    modify_dummy_component_state (put_me_on_clipboard);
    bonobo_clipboard_set_stream (put_me_on_clipboard,
				 CORBA_OBJECT_NIL,
				 &ev);
    Bonobo_Unknown_unref (put_me_on_clipboard, &ev);
    printf ("Done\n");
    
    printf ("Cleaning up\n");
    Bonobo_Unknown_unref (clipboard, &ev);
    CORBA_exception_free (&ev);

}

static void do_xclipboard_test ()
{
    CORBA_Environment     ev;
    Bonobo_PersistStream  xclipboard;
    BonoboStream         *memstream;
    Bonobo_Stream         stream;
    char                 *clipboard_contents;
    
    CORBA_exception_init (&ev);

    xclipboard = get_xclipboard_moniker ();

    g_return_if_fail (xclipboard != CORBA_OBJECT_NIL);



    g_warning ("Getting current clipboard");
    
    memstream = bonobo_stream_mem_create (NULL,
					  1024,
					  FALSE, TRUE);

    stream = bonobo_object_corba_objref (BONOBO_OBJECT (memstream));

    Bonobo_PersistStream_save (xclipboard, stream, "", &ev);
    Bonobo_Stream_seek (stream, 0, Bonobo_Stream_SEEK_SET, &ev);    
    bonobo_stream_client_read_string (stream, &clipboard_contents, &ev);
    
    g_warning ("Clipboard is %s", clipboard_contents);
    g_free (clipboard_contents);



    g_warning ("Setting X clipboard state");
    
    Bonobo_Stream_seek (stream, 0, Bonobo_Stream_SEEK_SET, &ev);    
    bonobo_stream_client_write_string (stream, "New Clipboard Text", TRUE, &ev);
    Bonobo_Stream_commit (stream, &ev);
    Bonobo_Stream_seek (stream, 0, Bonobo_Stream_SEEK_SET, &ev);    
    Bonobo_PersistStream_load (xclipboard, stream, "text/plain", &ev);

    g_warning ("Done");
    


    Bonobo_Unknown_unref (xclipboard, &ev);
    
    CORBA_exception_free (&ev);    
}

static gboolean
start_test (gpointer user_data)
{
    do_bonobo_clipboard_test ();

    /* do_xclipboard_test (); */

    bonobo_main_quit ();
    
    return FALSE;
}

int main (int argc, char **argv)
{
    if (!bonobo_init (&argc, argv))
	g_error ("bonobo_init failed");

    g_idle_add (start_test, NULL);
    
    bonobo_main ();

    return 0;
}
