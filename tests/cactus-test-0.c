#include <config.h>

#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-persist-stream.h>
#include <bonobo/bonobo-stream-client.h>
#include <bonobo/bonobo-generic-factory.h>

static void
pstream_save_impl (BonoboPersistStream        *ps,
		   const Bonobo_Stream         stream,
		   Bonobo_Persist_ContentType  type,
		   void                       *closure,
		   CORBA_Environment          *ev)
{
	CORBA_Environment  my_ev;

	{
	    FILE *fd = fopen ("/tmp/foonotif", "a");
	    fprintf (fd, "cactus-test-0: pstream_save\n");
	    fclose (fd);
	}

	CORBA_exception_init (&my_ev);
	bonobo_stream_client_write_string (stream,
					   *(gchar**) closure,
					   TRUE,
					   &my_ev);
	CORBA_exception_free (&my_ev);
}

static void
pstream_load_impl (BonoboPersistStream        *ps,
		   const Bonobo_Stream         stream,
		   Bonobo_Persist_ContentType  type,
		   void                       *closure,
		   CORBA_Environment          *ev)
{
	CORBA_Environment  my_ev;
	gchar             *contents;
	char             **state = closure;

	{
	    FILE *fd = fopen ("/tmp/foonotif", "a");
	    fprintf (fd, "cactus-test-0: pstream_load\n");
	    fclose (fd);
	}

	CORBA_exception_init (&my_ev);
	bonobo_stream_client_read_string (stream,
					  &contents,
					  &my_ev);
	CORBA_exception_free (&my_ev);

	if (*state)
	    g_free (*state);

	*state = g_strdup (contents);
	g_free (contents);
	
	{
	    FILE *fd = fopen ("/tmp/foonotif", "a");
	    fprintf (fd, "cactus-test-0: state is `%s'\n", *state);
	    fclose (fd);
	}
}

static Bonobo_Persist_ContentTypeList *
pstream_types_impl (BonoboPersistStream *stream,
		    void                *closure,
		    CORBA_Environment   *ev)
{
    return bonobo_persist_generate_content_types (1, "text/plain");
}

static BonoboObject *
create_hello ()
{
    gchar               **closure = g_new0 (gchar *, 1);
    BonoboPersistStream  *pstream = bonobo_persist_stream_new (pstream_load_impl,
							       pstream_save_impl,
							       pstream_types_impl,
							       "OAFIID:Cactus_Test_0",
							       (gpointer)closure);

    *closure = g_strdup ("Initial State");
    
    return BONOBO_OBJECT (pstream);
}

static BonoboObject *
factory_cb (BonoboGenericFactory *factory,
	    const gchar          *iid,
	    gpointer              closure)
{
    return create_hello ();
}

BONOBO_OAF_FACTORY ("OAFIID:Cactus_Test_Factory",
		    "", "0",
		    factory_cb, NULL);
		    
