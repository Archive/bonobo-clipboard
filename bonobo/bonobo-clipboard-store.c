/* -*- mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * Bonobo clipboard store implementation
 *
 * Author: �RDI Gerg� <cactus@cactus.rulez.org>
 *
 * Copyright 2001 Gerg� �rdi
 */

#include <bonobo/bonobo-clipboard-store.h>

#include <config.h>

#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-exception.h>

#include <bonobo/bonobo-stream-memory.h>
#include <bonobo/bonobo-storage-memory.h>
#include <bonobo/bonobo-moniker-util.h>
#include <bonobo/bonobo-moniker.h>

#include <bonobo/bonobo-clipboard-util.h>

static BonoboObjectClass  *bonobo_clipboard_store_parent_class;
static BonoboMonikerClass *bonobo_clipboard_store_moniker_parent_class;

struct _BonoboClipboardStorePriv {

        Bonobo_Moniker  source_moniker;

	Bonobo_Stream   source_state_stream;
	Bonobo_Storage  source_state_storage;

	Bonobo_Unknown  cached_object;

	char           *mime_type;
};

#define BONOBO_CLIPBOARD_STORE_MONIKER_TYPE        (bonobo_clipboard_store_moniker_get_type ())
#define BONOBO_CLIPBOARD_STORE_MONIKER(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), BONOBO_CLIPBOARD_STORE_MONIKER_TYPE, BonoboClipboardStoreMoniker))
#define BONOBO_CLIPBOARD_STORE_MONIKER_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), BONOBO_CLIPBOARD_STORE_MONIKER_TYPE, BonoboClipboardStoreMonikerClass))
#define BONOBO_IS_CLIPBOARD_STORE_MONIKER(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), BONOBO_CLIPBOARD_STORE_MONIKER_TYPE))
#define BONOBO_IS_CLIPBOARD_STORE_MONIKER_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), BONOBO_CLIPBOARD_STORE_MONIKER_TYPE))

typedef struct {
	BonoboMoniker         parent;

	BonoboClipboardStore *cstore;
} BonoboClipboardStoreMoniker;

typedef struct {
	BonoboMonikerClass    parent_class;
} BonoboClipboardStoreMonikerClass;

static void            bonobo_clipboard_store_store_moniker   (BonoboClipboardStore        *cstore,
							       const Bonobo_Persist         persist);

static void            bonobo_clipboard_store_clear           (BonoboClipboardStore        *cstore);

static Bonobo_Unknown  bonobo_clipboard_store_restore         (BonoboClipboardStore        *cstore);
static Bonobo_Unknown  bonobo_clipboard_store_restore_stream  (BonoboClipboardStore        *cstore);
static Bonobo_Unknown  bonobo_clipboard_store_restore_storage (BonoboClipboardStore        *cstore);

static void            bonobo_clipboard_store_set_clipboard   (BonoboClipboardStore        *cstore,
							       Bonobo_Moniker               linking_moniker);

static Bonobo_Unknown  bonobo_clipboard_store_moniker_resolve (BonoboMoniker               *moniker,
							       const Bonobo_ResolveOptions *options,
							       const CORBA_char            *requested_interface,
							       CORBA_Environment           *ev);

static GType           bonobo_clipboard_store_moniker_get_type (void);
static BonoboMoniker * bonobo_clipboard_store_moniker_new      (BonoboClipboardStore       *cstore);

static void
bonobo_clipboard_store_clear (BonoboClipboardStore *cstore)
{
	if (cstore->priv->cached_object)
	{
		bonobo_object_release_unref (cstore->priv->cached_object, NULL);
		cstore->priv->cached_object = CORBA_OBJECT_NIL;
	}
	
	if (cstore->priv->source_moniker)
	{
		CORBA_Environment ev;

		CORBA_exception_init (&ev);
		Bonobo_Unknown_unref (cstore->priv->source_moniker, &ev);
		CORBA_exception_free (&ev);

		cstore->priv->source_moniker = CORBA_OBJECT_NIL;
	}

	if (cstore->priv->source_state_stream)
	{
		CORBA_Environment ev;

		CORBA_exception_init (&ev);
		Bonobo_Unknown_unref (cstore->priv->source_state_stream, &ev);
		CORBA_exception_free (&ev);

		cstore->priv->source_state_stream = CORBA_OBJECT_NIL;
	}

	if (cstore->priv->source_state_storage)
	{
		CORBA_Environment ev;

		CORBA_exception_init (&ev);
		Bonobo_Unknown_unref (cstore->priv->source_state_storage, &ev);
		CORBA_exception_free (&ev);

		cstore->priv->source_state_storage = CORBA_OBJECT_NIL;
	}

	if (cstore->priv->mime_type)
	{
		g_free (cstore->priv->mime_type);
		cstore->priv->mime_type = 0;
	}
}

static void
bonobo_clipboard_store_store_moniker (BonoboClipboardStore *cstore,
				      const Bonobo_Persist  persist)
{
	char              *iid;
	Bonobo_Moniker     moniker;
	CORBA_Environment  ev;

	CORBA_exception_init (&ev);
	iid = Bonobo_Persist_getIId (persist, &ev);
	moniker = bonobo_moniker_client_new_from_name (iid, &ev);
	CORBA_exception_free (&ev);
	
	cstore->priv->source_moniker = moniker;
}

static Bonobo_Unknown
bonobo_clipboard_store_restore (BonoboClipboardStore *cstore)
{
	Bonobo_Unknown object = cstore->priv->cached_object;

	if (object)
		return bonobo_object_dup_ref (object, NULL);
	
	if (cstore->priv->source_state_stream != CORBA_OBJECT_NIL)
		object = bonobo_clipboard_store_restore_stream (cstore);
	else
		object = bonobo_clipboard_store_restore_storage (cstore);

	cstore->priv->cached_object = bonobo_object_dup_ref (object, NULL);
	
	return object;
}

static Bonobo_Unknown
bonobo_clipboard_store_restore_stream (BonoboClipboardStore *cstore)
{
	CORBA_Environment ev;
	Bonobo_PersistStream pstream = CORBA_OBJECT_NIL;

	CORBA_exception_init (&ev);

	pstream = Bonobo_Moniker_resolve (cstore->priv->source_moniker,
					  CORBA_OBJECT_NIL,
					  "IDL:Bonobo/PersistStream:1.0",
					  &ev);
	if (!pstream || BONOBO_EX (&ev))
		goto out;

	/* Rewind stream */
	Bonobo_Stream_seek (cstore->priv->source_state_stream,
			    Bonobo_Stream_SEEK_SET, 0, &ev);

	/* Restore state */
	Bonobo_PersistStream_load (pstream,
				   cstore->priv->source_state_stream,
				   cstore->priv->mime_type ?
				   cstore->priv->mime_type : "",
				   &ev);
 out:
	CORBA_exception_free (&ev);

	return pstream;
}

static Bonobo_Unknown
bonobo_clipboard_store_restore_storage (BonoboClipboardStore *cstore)
{
	CORBA_Environment ev;
	Bonobo_PersistStorage pstorage = CORBA_OBJECT_NIL;

	CORBA_exception_init (&ev);
	
	pstorage = Bonobo_Moniker_resolve (cstore->priv->source_moniker,
					   CORBA_OBJECT_NIL,
					   "IDL:Bonobo/PersistStorage:1.0",
					   &ev);
	if (!pstorage || BONOBO_EX (&ev))
		goto out;

	Bonobo_PersistStorage_load (pstorage,
				    cstore->priv->source_state_storage,
				    &ev);
 out:
	CORBA_exception_free (&ev);

	return pstorage;
}

static Bonobo_Unknown
bonobo_clipboard_store_moniker_resolve (BonoboMoniker               *moniker,
					const Bonobo_ResolveOptions *options,
					const CORBA_char            *interface,
					CORBA_Environment           *ev)
{
	BonoboClipboardStoreMoniker *cstore_moniker = BONOBO_CLIPBOARD_STORE_MONIKER (moniker);
	BonoboClipboardStore        *cstore = cstore_moniker->cstore;

	Bonobo_Moniker               parent;
	Bonobo_Unknown               object;

	parent = bonobo_moniker_get_parent (moniker, ev);
	if (BONOBO_EX (ev))
		return CORBA_OBJECT_NIL;
	    
        if (parent != CORBA_OBJECT_NIL)
	{
                bonobo_object_release_unref (parent, ev);
		
                g_warning ("clipboard_store moniker with a parent???");
		bonobo_exception_set (ev, ex_Bonobo_Moniker_InterfaceNotFound);
                return CORBA_OBJECT_NIL;
        }

	object = bonobo_clipboard_store_restore (cstore);
	if (object == CORBA_OBJECT_NIL)
	{
		bonobo_exception_set (ev, ex_Bonobo_Moniker_InterfaceNotFound);
		return CORBA_OBJECT_NIL;
	}

	return bonobo_moniker_util_qi_return (object, interface, ev);
		
}

static void
bonobo_clipboard_store_set_clipboard (BonoboClipboardStore *cstore,
					  Bonobo_Moniker        linking_moniker)
{
	BonoboMoniker *pasting_moniker;

	pasting_moniker = bonobo_clipboard_store_moniker_new (cstore);

	bonobo_clipboard_set (bonobo_object_corba_objref (BONOBO_OBJECT (pasting_moniker)),
			      linking_moniker,
			      NULL);
	
	bonobo_object_unref (BONOBO_OBJECT (pasting_moniker));
}

static void
fetch_stream (PortableServer_Servant      servant,
	      const Bonobo_PersistStream  source,
	      const Bonobo_Moniker        linking_moniker,
	      CORBA_Environment          *ev)
{
	BonoboClipboardStore           *cstore = 0;
	BonoboObject                   *stream_mem = 0;
	Bonobo_Stream                   stream;
	Bonobo_Persist_ContentTypeList *mime_types = 0;
	CORBA_Environment               my_ev;

	cstore = BONOBO_CLIPBOARD_STORE (bonobo_object_from_servant (servant));
	
	bonobo_clipboard_store_clear (cstore);
	bonobo_clipboard_store_store_moniker (cstore, source);

	stream_mem = bonobo_stream_mem_create (NULL, 0, FALSE, TRUE);
	stream = bonobo_object_corba_objref (stream_mem);
	cstore->priv->source_state_stream = stream;
	
	CORBA_exception_init (&my_ev);
	mime_types = Bonobo_Persist_getContentTypes (source, &my_ev);
	Bonobo_PersistStream_save (source, stream, "", &my_ev);
	CORBA_exception_free (&my_ev);

	if (mime_types)
	{
		if (mime_types->_length)
			cstore->priv->mime_type = g_strdup (mime_types->_buffer[0]);
		CORBA_free (mime_types);
	}

	bonobo_clipboard_store_set_clipboard (cstore, linking_moniker);

	{
		FILE *fd = fopen ("/tmp/foonotif", "a");
		fprintf (fd,
			 "bonobo-clipboard-store:"
			 "fetch_stream chk #7\n");
		fclose (fd);
	}
}

static void
fetch_storage (PortableServer_Servant       servant,
	       const Bonobo_PersistStorage  source,
	       const Bonobo_Moniker         linking_moniker,
	       CORBA_Environment           *ev)
{
	BonoboClipboardStore           *cstore;
	BonoboObject                   *storage_mem;
	Bonobo_Storage                  storage;
	CORBA_Environment               my_ev;

	cstore = BONOBO_CLIPBOARD_STORE (bonobo_object_from_servant (servant));
	
	bonobo_clipboard_store_clear (cstore);

	bonobo_clipboard_store_store_moniker (cstore, source);

	storage_mem = bonobo_storage_mem_create ();
	storage = bonobo_object_corba_objref (storage_mem);
	cstore->priv->source_state_storage = storage;
	
	CORBA_exception_init (&my_ev);
	Bonobo_PersistStorage_save (source, storage, FALSE, &my_ev);
	CORBA_exception_free (&my_ev);

	bonobo_clipboard_store_set_clipboard (cstore, linking_moniker);
}


/*
 * BonoboClipboardStore GObject code
 */
static void
bonobo_clipboard_store_finalize (GObject *object)
{
	BonoboClipboardStore *cstore = BONOBO_CLIPBOARD_STORE (object);

	/* Disown current monikers */
	bonobo_clipboard_store_clear (cstore);
	
	/* Free up instance structure */
	g_free (cstore->priv);

	/* Call parent finalize handler */	
	G_OBJECT_CLASS (bonobo_clipboard_store_parent_class)->finalize (object);
}


static void
bonobo_clipboard_store_class_init (BonoboClipboardStoreClass *klass)
{
	GObjectClass *object_class = (GObjectClass *)klass;
	POA_Bonobo_ClipboardStore__epv *epv = &klass->epv;

	bonobo_clipboard_store_parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = bonobo_clipboard_store_finalize;

	epv->fetchStream  = fetch_stream;
	epv->fetchStorage = fetch_storage;
}

static void
bonobo_clipboard_store_init (BonoboClipboardStore *cstore)
{
	cstore->priv = g_new0 (BonoboClipboardStorePriv, 1);

	/* This is not necessary because of the g_new0 usage */
	/*
	  cstore->priv->source_moniker       = CORBA_OBJECT_NIL;
	  cstore->priv->source_state_stream  = CORBA_OBJECT_NIL;
	  cstore->priv->source_state_storage = CORBA_OBJECT_NIL;
	  cstore->priv->cached_object        = CORBA_OBJECT_NIL;
	  cstore->priv->mime_type            = 0;
	*/
}

BonoboClipboardStore *
bonobo_clipboard_store_new ()
{
	BonoboClipboardStore *cstore;

	cstore = BONOBO_CLIPBOARD_STORE (g_object_new (BONOBO_CLIPBOARD_STORE_TYPE, NULL));
	
	return cstore;
}

BONOBO_TYPE_FUNC_FULL (BonoboClipboardStore,
		       Bonobo_ClipboardStore,
		       BONOBO_OBJECT_TYPE,
		       bonobo_clipboard_store);

/*
 * BonoboClipboardStoreMoniker GObject code
 */
static void
bonobo_clipboard_store_moniker_finalize (GObject *object)
{
	BonoboClipboardStoreMoniker *cstore_moniker = BONOBO_CLIPBOARD_STORE_MONIKER (object);

	/* Free clipboard store data */
	if (cstore_moniker->cstore)
	{
		bonobo_clipboard_store_clear (cstore_moniker->cstore);
		cstore_moniker->cstore = 0;
	}

	/* Call parent finalize handler */	
	G_OBJECT_CLASS (bonobo_clipboard_store_moniker_parent_class)->finalize (object);
}

static void
bonobo_clipboard_store_moniker_class_init (BonoboClipboardStoreMonikerClass *klass)
{
	GObjectClass *object_class = (GObjectClass *)klass;
	BonoboMonikerClass *moniker_class = BONOBO_MONIKER_CLASS (klass);

	bonobo_clipboard_store_moniker_parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = bonobo_clipboard_store_moniker_finalize;

	moniker_class->resolve = bonobo_clipboard_store_moniker_resolve;
}

static void
bonobo_clipboard_store_moniker_init (BonoboClipboardStoreMoniker *cstore_moniker)
{
	cstore_moniker->cstore = 0;
}

static BonoboMoniker *
bonobo_clipboard_store_moniker_construct (BonoboClipboardStoreMoniker *cstore_moniker,
					  BonoboClipboardStore        *cstore)
{
	g_return_val_if_fail (cstore_moniker != NULL, NULL);
	g_return_val_if_fail (cstore != NULL, NULL);

	cstore_moniker->cstore= cstore;
	
	return bonobo_moniker_construct (BONOBO_MONIKER (cstore_moniker),
					 "clipboard_store:");
}

static BonoboMoniker *
bonobo_clipboard_store_moniker_new (BonoboClipboardStore *cstore)
{
	BonoboClipboardStoreMoniker *cstore_moniker = 0;
	
	cstore_moniker = g_object_new (BONOBO_CLIPBOARD_STORE_MONIKER_TYPE, NULL);

	return bonobo_clipboard_store_moniker_construct (cstore_moniker, cstore);
}

BONOBO_TYPE_FUNC (BonoboClipboardStoreMoniker,
		  BONOBO_MONIKER_TYPE,
		  bonobo_clipboard_store_moniker);
