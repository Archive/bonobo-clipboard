/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * Bonobo clipboard implementation
 *
 * Author: �RDI Gerg� <cactus@cactus.rulez.org>
 *
 * Copyright 2001 Gerg� �rdi
 */

#ifndef BONOBO_CLIPBOARD_H
#define BONOBO_CLIPBOARD_H

#include <bonobo/bonobo-object.h>

G_BEGIN_DECLS

#define BONOBO_CLIPBOARD_TYPE        (bonobo_clipboard_get_type ())
#define BONOBO_CLIPBOARD(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), BONOBO_CLIPBOARD_TYPE, BonoboClipboard))
#define BONOBO_CLIPBOARD_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), BONOBO_CLIPBOARD_TYPE, BonoboClipboardClass))
#define BONOBO_IS_CLIPBOARD(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), BONOBO_CLIPBOARD_TYPE))
#define BONOBO_IS_CLIPBOARD_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), BONOBO_CLIPBOARD_TYPE))

typedef struct _BonoboClipboardPrivate BonoboClipboardPrivate;

typedef struct {
	BonoboObject base;

	BonoboClipboardPrivate *priv;
} BonoboClipboard;

typedef struct {
	BonoboObjectClass parent_class;

	POA_Bonobo_Clipboard__epv epv;
} BonoboClipboardClass;

GType bonobo_clipboard_get_type    (void);

G_END_DECLS

#endif /* BONOBO_CLIPBOARD_H */
