/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * Bonobo clipboard store implementation
 *
 * Author: �RDI Gerg� <cactus@cactus.rulez.org>
 *
 * Copyright 2001 Gerg� �rdi
 */

#ifndef BONOBO_CLIPBOARD_STORE_H
#define BONOBO_CLIPBOARD_STORE_H

#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-moniker.h>

G_BEGIN_DECLS
 
#define BONOBO_CLIPBOARD_STORE_TYPE        (bonobo_clipboard_store_get_type ())
#define BONOBO_CLIPBOARD_STORE(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), BONOBO_CLIPBOARD_STORE_TYPE, BonoboClipboardStore))
#define BONOBO_CLIPBOARD_STORE_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), BONOBO_CLIPBOARD_STORE_TYPE, BonoboClipboardStoreClass))
#define BONOBO_IS_CLIPBOARD_STORE(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), BONOBO_CLIPBOARD_STORE_TYPE))
#define BONOBO_IS_CLIPBOARD_STORE_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), BONOBO_CLIPBOARD_STORE_TYPE))

typedef struct _BonoboClipboardStorePriv BonoboClipboardStorePriv;

typedef struct {
	BonoboObject base;

	BonoboClipboardStorePriv *priv;
} BonoboClipboardStore;

typedef struct {
	BonoboObjectClass parent_class;

	POA_Bonobo_ClipboardStore__epv epv;
} BonoboClipboardStoreClass;

GType                  bonobo_clipboard_store_get_type  (void);
BonoboClipboardStore * bonobo_clipboard_store_new       (void);

G_END_DECLS

#endif /* BONOBO_CLIPBOARD_STORE_H */
