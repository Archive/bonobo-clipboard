/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * Bonobo clipboard client-side utility functions
 *
 * Author: �RDI Gerg� <cactus@cactus.rulez.org>
 *
 * Copyright 2001 Gerg� �rdi
 */

#ifndef BONOBO_CLIPBOARD_UTIL_H
#define BONOBO_CLIPBOARD_UTIL_H

#include <bonobo/Bonobo.h>

G_BEGIN_DECLS

void  bonobo_clipboard_set         (Bonobo_Moniker     pasting,
				    Bonobo_Moniker     linking,
				    CORBA_Environment *opt_ev);

void  bonobo_clipboard_set_stream  (Bonobo_Unknown     source,
				    Bonobo_Moniker     link,
				    CORBA_Environment *opt_ev);

void  bonobo_clipboard_set_storage (Bonobo_Unknown     source,
				    Bonobo_Moniker     link,
				    CORBA_Environment *opt_ev);

Bonobo_Moniker bonobo_clipboard_paste (CORBA_Environment *opt_ev);
Bonobo_Moniker bonobo_clipboard_link  (CORBA_Environment *opt_ev);

G_END_DECLS

#endif /* BONOBO_CLIPBOARD_UTIL_H */
