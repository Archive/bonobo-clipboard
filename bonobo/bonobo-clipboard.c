/* -*- mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * Bonobo clipboard store implementation
 *
 * Author: �RDI Gerg� <cactus@cactus.rulez.org>
 *
 * Copyright 2001 Gerg� �rdi
 */

#include <bonobo/bonobo-clipboard.h>

#include <config.h>
#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-event-source.h>
#include <bonobo/bonobo-arg.h>
#include <bonobo/bonobo-moniker-util.h>
#include <bonobo/bonobo-stream-memory.h>

#include <gtk/gtkclipboard.h>

static void   bonobo_clipboard_clear              (BonoboClipboard  *clipboard);
static void   bonobo_clipboard_emit_notifications (BonoboClipboard  *clipboard);

static void   bonobo_clipboard_xproxy_set         (BonoboClipboard  *clipboard);
static GList* bonobo_clipboard_xproxy_targets     (BonoboClipboard  *clipboard);
static void   bonobo_clipboard_xproxy_get_cb      (GtkClipboard     *clipboard,
						   GtkSelectionData *selection_data,
						   guint             info,
						   gpointer          user_data);
static void   bonobo_clipboard_xproxy_clear_cb    (GtkClipboard     *clipboard,
						   gpointer          user_data);

#define PARENT_TYPE BONOBO_OBJECT_TYPE

/* Parent object class in GTK hierarchy */
static BonoboObjectClass *bonobo_clipboard_parent_class;

struct _BonoboClipboardPrivate {
	BonoboEventSource *eventsource;
	
	Bonobo_Moniker     pasting_moniker;
	Bonobo_Moniker     linking_moniker;
};

static void
bonobo_clipboard_finalize (GObject *object)
{
	BonoboClipboard *clipboard = BONOBO_CLIPBOARD (object);

	/* Disown current monikers */
	bonobo_clipboard_clear (clipboard);

	/* Fire notification */
	bonobo_clipboard_emit_notifications (clipboard);
	
	/* Destroy eventsource */
	bonobo_object_unref (BONOBO_OBJECT (clipboard->priv->eventsource));

	/* Free up instance structure */
	g_free (clipboard->priv);

	/* Call parent finalize handler */	
	G_OBJECT_CLASS (bonobo_clipboard_parent_class)->finalize (object);
}

static void
impl_Bonobo_Clipboard_setClipboard (PortableServer_Servant  servant,
				    Bonobo_Moniker          pasting_moniker,
				    Bonobo_Moniker          linking_moniker,
				    CORBA_Environment      *ev)
{
	BonoboClipboard *clipboard = BONOBO_CLIPBOARD (bonobo_object_from_servant (servant));

	/* Empty current state */
	bonobo_clipboard_clear (clipboard);

	/* Store the monikers */
	if (pasting_moniker != CORBA_OBJECT_NIL)
		clipboard->priv->pasting_moniker = bonobo_object_dup_ref (pasting_moniker, NULL);

	if (linking_moniker != CORBA_OBJECT_NIL)
		clipboard->priv->linking_moniker = bonobo_object_dup_ref (linking_moniker, NULL);

	/* Set X clipboard */
	bonobo_clipboard_xproxy_set (clipboard);
	
	/* Fire an event */
	bonobo_clipboard_emit_notifications (clipboard);	
}

static Bonobo_Moniker
impl_Bonobo_Clipboard_paste (PortableServer_Servant  servant,
			     CORBA_Environment      *ev)
{
	CORBA_Environment my_ev;
	Bonobo_Moniker    retval = CORBA_OBJECT_NIL;
	
	BonoboClipboard *clipboard = BONOBO_CLIPBOARD (bonobo_object_from_servant (servant));

	if (clipboard->priv->pasting_moniker != CORBA_OBJECT_NIL) {
		CORBA_exception_init (&my_ev);
		retval = bonobo_object_dup_ref (clipboard->priv->pasting_moniker, &my_ev);
		CORBA_exception_free (&my_ev);
	}
	
	return retval;
}

static Bonobo_Moniker
impl_Bonobo_Clipboard_link (PortableServer_Servant  servant,
			    CORBA_Environment      *ev)
{
	CORBA_Environment my_ev;

	BonoboClipboard *clipboard = BONOBO_CLIPBOARD (bonobo_object_from_servant (servant));

	if (clipboard->priv->linking_moniker != CORBA_OBJECT_NIL) {
		CORBA_exception_init (&my_ev);
		Bonobo_Unknown_ref (clipboard->priv->linking_moniker, &my_ev);
		CORBA_exception_free (&my_ev);
	}

	return clipboard->priv->linking_moniker;
}

static void
bonobo_clipboard_class_init (BonoboClipboardClass *klass)
{
	GObjectClass *object_class = (GObjectClass *)klass;
	POA_Bonobo_Clipboard__epv *epv = &klass->epv;

	bonobo_clipboard_parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = bonobo_clipboard_finalize;

	epv->setClipboard = impl_Bonobo_Clipboard_setClipboard;
	epv->paste = impl_Bonobo_Clipboard_paste;
	epv->link = impl_Bonobo_Clipboard_link;
}

static void
bonobo_clipboard_init (BonoboClipboard *clipboard)
{
	clipboard->priv = g_new0 (BonoboClipboardPrivate, 1);

	clipboard->priv->pasting_moniker = CORBA_OBJECT_NIL;
	clipboard->priv->linking_moniker = CORBA_OBJECT_NIL;

	clipboard->priv->eventsource = bonobo_event_source_new ();
	bonobo_object_add_interface (BONOBO_OBJECT (clipboard),
				     BONOBO_OBJECT (clipboard->priv->eventsource));
}

BONOBO_TYPE_FUNC_FULL (BonoboClipboard, 
		       Bonobo_Clipboard,
		       PARENT_TYPE,
		       bonobo_clipboard);


static void
bonobo_clipboard_clear (BonoboClipboard *clipboard)
{
	CORBA_Environment ev;
	
	Bonobo_Moniker pmoniker = clipboard->priv->pasting_moniker;
	Bonobo_Moniker lmoniker = clipboard->priv->linking_moniker;

	if (pmoniker != CORBA_OBJECT_NIL) {
		CORBA_exception_init (&ev);
		Bonobo_Unknown_unref (pmoniker, &ev);
		CORBA_exception_free (&ev);

		clipboard->priv->pasting_moniker = CORBA_OBJECT_NIL;
	}

	if (lmoniker != CORBA_OBJECT_NIL) {
		CORBA_exception_init (&ev);
		Bonobo_Unknown_unref (lmoniker, &ev);
		CORBA_exception_free (&ev);

		clipboard->priv->linking_moniker = CORBA_OBJECT_NIL;
	}
}

static void
bonobo_clipboard_emit_notifications (BonoboClipboard *clipboard)
{
	BonoboArg *val;
	char *eventname;

	/* First, a generic `changed' event is fired */
	eventname = bonobo_event_make_name ("Bonobo/Clipboard", 
					    "changed", 
					    NULL);
	bonobo_event_source_notify_listeners (clipboard->priv->eventsource,
					      eventname,
					      NULL,
					      NULL);
	g_free (eventname);

#if 0	
	/* Then, two events containing the new monikers are emitted */
	eventname = bonobo_event_make_name ("Bonobo/Clipboard", 
					    "changed", 
					    "pasting");
	val = bonobo_arg_new (TC_Bonobo_Moniker);
	val->_value = clipboard->priv->pasting_moniker;
	bonobo_event_source_notify_listeners (clipboard->priv->eventsource,
					      eventname,
					      val,
					      NULL);
	bonobo_arg_release (val);
	g_free (eventname);

	eventname = bonobo_event_make_name ("Bonobo/Clipboard", 
					    "changed", 
					    "linking");
	val = bonobo_arg_new (TC_Bonobo_Moniker);
	val->_value = clipboard->priv->linking_moniker;
	bonobo_event_source_notify_listeners (clipboard->priv->eventsource,
					      eventname,
					      val,
					      NULL);
	bonobo_arg_release (val);
	g_free (eventname);
#endif
}

static void
bonobo_clipboard_xproxy_set (BonoboClipboard *clipboard)
{
	GtkClipboard   *x_clipboard;
	GtkTargetEntry *x_targets;
	GList          *target_list, *target_list_i;
	int             target_list_length;
	int             i;
    
	target_list = bonobo_clipboard_xproxy_targets (clipboard);
	if (!target_list)
		return;
	
	target_list_length = g_list_length (target_list);
	if (!target_list_length) {
		g_list_free (target_list);
		return;
	}
	
	
	x_targets = g_new0 (GtkTargetEntry, target_list_length);
	for (i = 0, target_list_i = target_list; target_list_i;
	     target_list_i = target_list_i->next, i++)
		x_targets[i].target = target_list_i->data;
		
	x_clipboard = gtk_clipboard_get (GDK_SELECTION_PRIMARY);
#warning Should use GDK_NONE here

	gtk_clipboard_set_with_data  (x_clipboard,
				      x_targets, target_list_length,
				      bonobo_clipboard_xproxy_get_cb,
				      bonobo_clipboard_xproxy_clear_cb,
				      clipboard);
	
	g_list_foreach (target_list, (GFunc) g_free, NULL);
	g_list_free (target_list);

	g_free (x_targets);
}

/*
 * The returned list's entries and the list itself has to be free'd
 */
static GList *
bonobo_clipboard_xproxy_targets (BonoboClipboard *clipboard)
{
	Bonobo_Persist                  persist;
	CORBA_Environment               ev;
	Bonobo_Persist_ContentTypeList *content_types = 0;
	int                             i;
	GList                          *target_list = 0;

	persist = bonobo_moniker_client_resolve_default
		(clipboard->priv->pasting_moniker,
		 "IDL:Bonobo/PersistStream:1.0", NULL);

	if (!persist)
		return NULL;
	
	CORBA_exception_init (&ev);	
	content_types = Bonobo_Persist_getContentTypes (persist, &ev);

	if (!persist || BONOBO_EX (&ev)) {
		CORBA_exception_free (&ev);
		return NULL;
	}

	for (i = 0; i < content_types->_length; i++)
	{
		target_list = g_list_prepend (target_list, g_strdup (content_types->_buffer[i]));
		if (!strcmp (content_types->_buffer[i], "text/plain")) {
		    target_list = g_list_prepend (target_list, g_strdup ("TEXT"));
		    target_list = g_list_prepend (target_list, g_strdup ("STRING"));
		}
	}
		
	CORBA_free (content_types);
	bonobo_object_release_unref (persist, NULL);
	
	return target_list;
}

static void bonobo_clipboard_xproxy_get_cb (GtkClipboard     *x_clipboard,
					    GtkSelectionData *selection_data,
					    guint             info,
					    gpointer          user_data)
{
	BonoboClipboard      *clipboard;
	CORBA_Environment     ev;
	char                 *content_type;
	BonoboStreamMem      *stream_mem;
	Bonobo_Stream         stream;
	Bonobo_PersistStream  persist;
	char                 *stream_contents;
	size_t                stream_size;

	selection_data->type   = 0;
	selection_data->length = 0; /* If an error occurs, return a clean selection */
	
	g_return_if_fail (user_data && BONOBO_IS_CLIPBOARD (user_data));

	clipboard = BONOBO_CLIPBOARD (user_data);
	
	content_type = gdk_atom_name (selection_data->target);
	if (!strcmp (content_type, "STRING") ||
	    !strcmp (content_type, "TEXT")) {
		g_free (content_type);
		content_type = g_strdup ("text/plain");
	}

	stream_mem = BONOBO_STREAM_MEM (bonobo_stream_mem_create (NULL, 0, FALSE, TRUE));
	stream = bonobo_object_corba_objref (BONOBO_OBJECT (stream_mem));
	
	persist = bonobo_moniker_client_resolve_default
		(clipboard->priv->pasting_moniker,
		 "IDL:Bonobo/PersistStream:1.0", NULL);
	if (!persist)
		goto out;

	CORBA_exception_init (&ev);
	Bonobo_PersistStream_save (persist, stream, content_type, &ev);
	if (BONOBO_EX (&ev)) {
		CORBA_exception_free (&ev);
		goto out;
	}

	stream_size = bonobo_stream_mem_get_size (stream_mem);
	stream_contents = g_memdup (bonobo_stream_mem_get_buffer (stream_mem),
				    stream_size);
	
	/* Everything is ready to fill up the selection */
	selection_data->type   = selection_data->target;
	selection_data->format = 8; /* bytes */
	selection_data->data   = stream_contents;
	selection_data->length = stream_size;

 out:
	if (stream_mem)
		bonobo_object_unref (BONOBO_OBJECT (stream_mem));
	if (persist)
		bonobo_object_release_unref (persist, NULL);
	
	g_free (content_type);
}

static void bonobo_clipboard_xproxy_clear_cb (GtkClipboard     *x_clipboard,
					      gpointer          user_data)
{
}
