/* -*- mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * X-to-Bonobo clipboard bridge
 *
 * Author: �RDI Gerg� <cactus@cactus.rulez.org>
 *
 * Copyright 2001 Gerg� �rdi
 */

#include <config.h>

#include <bonobo/bonobo-moniker.h>
#include <bonobo/bonobo-moniker-util.h>
#include <bonobo/bonobo-moniker-simple.h>
#include <bonobo/bonobo-exception.h>

#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-generic-factory.h>

#include "bonobo/bonobo-clipboard.h"
#include "bonobo/bonobo-clipboard-store.h"

#define CLIPBOARD_IID         "OAFIID:Bonobo_Clipboard"
#define CLIPBOARD_MONIKER_IID "OAFIID:Bonobo_Moniker_Clipboard"
#define CLIPBOARD_STORE_IID   "OAFIID:Bonobo_Clipboard_Store"
#define FACTORY_IID           "OAFIID:Bonobo_Clipboard_Factory"

static BonoboClipboard *clipboard_instance = 0;

static Bonobo_Unknown
clipboard_resolve (BonoboMoniker               *moniker,
		   const Bonobo_ResolveOptions *options,
		   const CORBA_char            *requested_interface,
		   CORBA_Environment           *ev)
{
	Bonobo_Unknown ret = bonobo_object_corba_objref (BONOBO_OBJECT (clipboard_instance));	
	
	if (!strcmp (requested_interface, "IDL:Bonobo/Clipboard:1.0"))
		return bonobo_object_dup_ref (ret, ev);
	
	if (!strcmp (requested_interface, "IDL:Bonobo/EventSource:1.0"))
		return bonobo_moniker_util_qi_return (ret, requested_interface, ev);
	
	bonobo_exception_set (ev, ex_Bonobo_Moniker_InterfaceNotFound);
	
	return CORBA_OBJECT_NIL;
}

static BonoboMoniker *
clipboard_moniker_new ()
{
	return bonobo_moniker_simple_new ("clipboard:", clipboard_resolve);
}

static BonoboObject *
clipboard_factory (BonoboGenericFactory *factory,
		   const gchar          *iid,
		   gpointer            	user_data)
{
	static BonoboObject *clipboard_moniker_instance = 0;
	static BonoboObject *cstore_instance = 0;
	
	if (!clipboard_instance) {
		clipboard_instance = g_object_new (BONOBO_CLIPBOARD_TYPE, NULL);
		bonobo_object_set_imortal (BONOBO_OBJECT (clipboard_instance), TRUE);
	}
	
	if (!cstore_instance) {
		cstore_instance = BONOBO_OBJECT (bonobo_clipboard_store_new ());
		bonobo_object_set_imortal (cstore_instance, TRUE);
	}
	
	if (!clipboard_moniker_instance) {
		clipboard_moniker_instance = BONOBO_OBJECT (clipboard_moniker_new ());
		bonobo_object_set_imortal (clipboard_moniker_instance, TRUE);
	}
	
	if (!strcmp (iid, CLIPBOARD_IID))
		return BONOBO_OBJECT (clipboard_instance);

	if (!strcmp (iid, CLIPBOARD_STORE_IID))
		return cstore_instance;

	if (!strcmp (iid, CLIPBOARD_MONIKER_IID))
		return clipboard_moniker_instance;
		
	return NULL;
}

int main (int argc, char *argv [])
{
	gtk_init (&argc, &argv);
	
	if (!bonobo_init (&argc, argv))
		g_error ("bonobo_init failed");

        bonobo_generic_factory_new (FACTORY_IID, clipboard_factory, NULL);

        bonobo_main ();

        return 0;
}                                                                             

