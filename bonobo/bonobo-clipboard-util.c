/* -*- mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * Bonobo clipboard client-side utility functions
 *
 * Author: �RDI Gerg� <cactus@cactus.rulez.org>
 *
 * Copyright 2001 Gerg� �rdi
 */

#include <bonobo/bonobo-clipboard-util.h>

#include <config.h>

#include <bonobo/bonobo-moniker-util.h>
#include <bonobo/bonobo-exception.h>

static Bonobo_Clipboard
get_clipboard (CORBA_Environment *ev)
{
	return bonobo_get_object ("clipboard:",
				  "IDL:Bonobo/Clipboard:1.0",
				  ev);
}

static Bonobo_ClipboardStore
get_cstore (CORBA_Environment *ev)
{
	return bonobo_get_object ("OAFIID:Bonobo_Clipboard_Store",
				  "IDL:Bonobo/ClipboardStore:1.0",
				  ev);
}

void
bonobo_clipboard_set (Bonobo_Unknown     pasting,
		      Bonobo_Unknown     linking,
		      CORBA_Environment *opt_ev)
{
	Bonobo_Clipboard  clipboard;
	CORBA_Environment my_ev, *ev;

	if (opt_ev)
		ev = opt_ev;
	else
	{
		CORBA_exception_init (&my_ev);
		ev = &my_ev;
	}
	
	clipboard = get_clipboard (ev);
	if (BONOBO_EX (ev)) {
		if (!opt_ev)
		{
			g_warning ("file %s: line %d (%s): "
				   "Bonobo Clipboard activation failed",
				   __FILE__, __LINE__, __PRETTY_FUNCTION__);
			CORBA_exception_free (ev);
		}
		return;
	}

	Bonobo_Clipboard_setClipboard (clipboard, pasting, linking, ev);

	if (!opt_ev)
		CORBA_exception_free (ev);
}

void
bonobo_clipboard_set_stream (Bonobo_Unknown     source,
			     Bonobo_Moniker     link,
			     CORBA_Environment *opt_ev)
{
	Bonobo_ClipboardStore  cstore;
	Bonobo_PersistStream   pstream;
	CORBA_Environment      my_ev, *ev;

	if (opt_ev)
		ev = opt_ev;
	else
	{
		CORBA_exception_init (&my_ev);
		ev = &my_ev;
	}

	pstream = Bonobo_Unknown_queryInterface (source,
						 "IDL:Bonobo/PersistStream:1.0",
						 ev);
	if (BONOBO_EX (ev)) {
		if (!opt_ev)
		{
			g_warning ("file %s: line %d (%s): "
				   "Bonobo::PersistStream interface not found",
				   __FILE__, __LINE__, __PRETTY_FUNCTION__);
			CORBA_exception_free (ev);
		}
		return;
	}
	
	cstore = get_cstore (ev);
	if (BONOBO_EX (ev)) {
		if (!opt_ev)
		{
			g_warning ("file %s: line %d (%s): "
				   "Bonobo ClipboardStore activation failed",
				   __FILE__, __LINE__, __PRETTY_FUNCTION__);
			CORBA_exception_free (ev);
		}
		return;
	}

	Bonobo_ClipboardStore_fetchStream (cstore, pstream, link, ev);
	if (!opt_ev)
		CORBA_exception_free (ev);
}

void
bonobo_clipboard_set_storage (Bonobo_Unknown     source,
			      Bonobo_Moniker     link,
			      CORBA_Environment *opt_ev)
{
	Bonobo_ClipboardStore  cstore;
	Bonobo_PersistStorage  pstorage;
	CORBA_Environment      my_ev, *ev;

	if (opt_ev)
		ev = opt_ev;
	else
	{
		CORBA_exception_init (&my_ev);
		ev = &my_ev;
	}

	pstorage = Bonobo_Unknown_queryInterface (source,
						  "IDL:Bonobo/PersistStorage:1.0",
						  ev);
	if (BONOBO_EX (ev)) {
		if (!opt_ev)
		{
			g_warning ("file %s: line %d (%s): "
				   "Bonobo::PersistStorage interface not found",
				   __FILE__, __LINE__, __PRETTY_FUNCTION__);
			CORBA_exception_free (ev);
		}
		return;
	}
	
	cstore = get_cstore (ev);
	g_return_if_fail (cstore != CORBA_OBJECT_NIL);

	Bonobo_ClipboardStore_fetchStorage (cstore, pstorage, link, ev);
	if (!opt_ev)
		CORBA_exception_free (ev);
}

Bonobo_Moniker
bonobo_clipboard_paste (CORBA_Environment *opt_ev)
{
	Bonobo_Moniker    moniker;
	Bonobo_Clipboard  clipboard;
	CORBA_Environment my_ev, *ev;

	if (opt_ev)
		ev = opt_ev;
	else
	{
		CORBA_exception_init (&my_ev);
		ev = &my_ev;
	}
	clipboard = get_clipboard (ev);
	if (BONOBO_EX (ev)) {
		if (!opt_ev)
		{
			g_warning ("file %s: line %d (%s): "
				   "Bonobo Clipboard activation failed",
				   __FILE__, __LINE__, __PRETTY_FUNCTION__);
			CORBA_exception_free (ev);
		}
		return CORBA_OBJECT_NIL;
	}
	
	moniker = Bonobo_Clipboard_paste (clipboard, ev);

	if (!opt_ev)
		CORBA_exception_free (ev);
	return moniker;
}

Bonobo_Moniker
bonobo_clipboard_link (CORBA_Environment *opt_ev)
{
	Bonobo_Moniker    moniker;
	Bonobo_Clipboard  clipboard;
	CORBA_Environment my_ev, *ev;

	if (opt_ev)
		ev = opt_ev;
	else
	{
		CORBA_exception_init (&my_ev);
		ev = &my_ev;
	}
	clipboard = get_clipboard (ev);
	if (BONOBO_EX (ev)) {
		if (!opt_ev)
		{
			g_warning ("file %s: line %d (%s): "
				   "Bonobo Clipboard activation failed",
				   __FILE__, __LINE__, __PRETTY_FUNCTION__);
			CORBA_exception_free (ev);
		}
		return CORBA_OBJECT_NIL;
	}
	
	moniker = Bonobo_Clipboard_link (clipboard, ev);

	if (!opt_ev)
		CORBA_exception_free (ev);
	return moniker;
}
